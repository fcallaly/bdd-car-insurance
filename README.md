A project demonstration BDD with behave and UI testing with selenium.

Contains tests for a web page that implements a simple chatbot.

When behave is installed, run the tests by running "behave" in the root directory.

## Install ##
Install requirements.txt: pip install -r requirements.txt

OR

pip install behave
pip install selenium
pip install pyhamcrest


The chromedriver executable facilitates controlling the chrome browser from code.

download latest chromedriver from https://chromedriver.storage.googleapis.com/index.html

Extract chromedriver and place file in a directory that's on the PATH or run behave from the same directory.
