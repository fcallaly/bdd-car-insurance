from behave import given, when, then
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from hamcrest import assert_that, starts_with
import time

HEADLESS = True
BOT_URL = 'https://benivade-chatbot.s3-eu-west-1.amazonaws.com/index.html'

@given(u'the user enters \'{intent}\'')
def step_impl(context, intent):
    webpage.load_chatbot()
    send_param(intent)


@when(u'asked for the car make the user enters \'{carMake}\'')
def step_impl(context, carMake):
    wait_and_send(carMake)


@when(u'when asked for the car model the user enters \'{carModel}\'')
def step_impl(context, carModel):
    wait_and_send(carModel)


@when(u'when asked for date of birth the user enters \'{dateOfBirth}\'')
def step_impl(context, dateOfBirth):
    wait_and_send(dateOfBirth)


@when(u'when asked for test date the user enters \'{testDate}\'')
def step_impl(context, testDate):
    wait_and_send(testDate)


@when(u'when asked for engine size the user enters \'{engineSize}\'')
def step_impl(context, engineSize):
    wait_and_send(engineSize)


@when(u'when prompted to confirm the user enters \'{confirm}\'')
def step_impl(context, confirm):
    wait_and_send(confirm)


@then(u'the chatbot should respond with \'{fulfillment}\'')
def step_impl(context, fulfillment):
    result_message = webpage.wait_for_next_message()

    # the chatbot returns "..." messages whilst it is 'thinking'
    while result_message.startswith('.'):
        time.sleep(2)

        # see if it has finished thinking yet
        result_message = webpage.get_current_message_again() 
    
    assert_that(result_message, starts_with(fulfillment))


def wait_and_send(param):
    webpage.wait_for_next_message()
    send_param(param)


def send_param(param):
    webpage.text_input_box.send_keys(param)
    webpage.text_input_box.send_keys(Keys.RETURN)
    webpage.text_input_box.clear()


class WebPage:

    instance = None

    def __init__(self):
       pass

    def load_chatbot(self):
        chrome_options = Options()
        
        if HEADLESS:
            chrome_options.add_argument('--headless')
        
         # options turn off the prompt for the microphone
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
       
        self.driver = webdriver.Chrome(chrome_options=chrome_options)
        self.website = self.driver.get(BOT_URL)
        self.number_of_messages = 1
        self.wait_for_next_message()
        self.text_input_box = self.driver.find_element_by_id('text-input')

    @classmethod
    def get_instance(cls):
        if cls.instance is None:
            cls.instance = WebPage()
        return cls.instance

    def wait_for_next_message(self):
        latest_message_from_bot = ''
        try:
            # this is a timeoout delay in seconds
            delay = 10
            css_selector = '.message-bot:nth-child(' + str(self.number_of_messages) + ')'
            print('waiting for ' + css_selector)
            WebDriverWait(self.driver, delay).until(EC.presence_of_element_located((By.CSS_SELECTOR, css_selector)))
            
            # needs a brief pause to allow the chatbot time to put some content in the field
            time.sleep(2)
            latest_message_element = self.driver.find_element_by_css_selector(css_selector=css_selector)
            latest_message_from_bot = latest_message_element.text
            print('message received: ' + latest_message_from_bot)

            # this is two because every other one is from the human
            self.number_of_messages = self.number_of_messages + 2

        except TimeoutException:
            print("Loading took too much time!")

        return latest_message_from_bot

    def get_current_message_again(self):
        css_selector = '.message-bot:nth-child(' + str(self.number_of_messages-2) + ')'
        print('trying again for ' + css_selector)

        latest_message_element = self.driver.find_element_by_css_selector(css_selector=css_selector)
        latest_message_from_bot = latest_message_element.text

        print('message received: ' + latest_message_from_bot)

        return latest_message_from_bot


webpage = WebPage.get_instance()
