Feature: Requesting an auto insurance quote from a chat bot
  Provided with the car details and user details the chatbot
  should respond with a rating

  Scenario: Get an Auto Insurance Quote for Toyota

    Given the user enters 'I would like an insurance quote'
    When asked for the car make the user enters 'toyota'
    And when asked for the car model the user enters 'yaris'
    And when asked for date of birth the user enters '2/3/88'
    And when asked for test date the user enters '4/4/2012'
    And when asked for engine size the user enters '1.1'
    And when prompted to confirm the user enters 'yes'
    Then the chatbot should respond with 'For driver 33 years of age who has been driving for 9 years driving a 1.1litre engine, the cost of insurance will be approximately: £99'


  Scenario: Get an Auto Insurance Quote for Ford

    Given the user enters 'I would like an insurance quote'
    When asked for the car make the user enters 'Ford'
    And when asked for the car model the user enters 'mondeo'
    And when asked for date of birth the user enters '11/11/66'
    And when asked for test date the user enters '4/7/86'
    And when asked for engine size the user enters '2.0'
    And when prompted to confirm the user enters 'yes'
    Then the chatbot should respond with 'For driver 55 years of age who has been driving for 35 years driving a 2.0litre engine, the cost of insurance will be approximately: £53'

